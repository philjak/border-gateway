package boarderGateway;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;
import com.fasterxml.jackson.dataformat.cbor.CBORParser;


public class CoAPRessources {
	

	public static class RegisterRessource extends CoapResource{
		private MySQL database;
    	public RegisterRessource(){
    		super("register");
    		this.database = new MySQL();
    
        	getAttributes().setTitle("Client Registrierung");
    	}

		
		@Override
		public void handlePOST(CoapExchange exchange) {			
			List<String> allowedTypes = Arrays.asList("string", "integer", "float");
			boolean errorOccurred = false;

			// TODO Auto-generated method stub
			CBORFactory f = new CBORFactory();
			String source_address = exchange.getSourceAddress().getHostAddress();
			System.out.println("Received POST on /register from "+source_address);
			// Check, if source IP is allowed to 
			if (database.isClientAllowed(source_address)){
				try {
					// Lade erhaltenen Bytecode in CBOR Parser
					byte payload[] = exchange.getRequestPayload();
					CBORParser myCborParser = f.createParser(payload);
		
					List<String> paras = new ArrayList<String>();
					List<String> types = new ArrayList<String>();
					
					// Gehe zum ersten Token. Überspringe erstes "["
					myCborParser.nextToken();
					myCborParser.nextToken();
					
					String name = myCborParser.getText();
					System.out.println("Name: "+name);
					
					// Lese Arrays aus: [ "Parameter Name" , "Datentyp"]
					String para = "";
					String type = "";
					
					// While Schleife, bis zum Ende der CBOR Nachricht 
					while (myCborParser.nextToken() != null){
						// Array Beginn
						if (myCborParser.getText().equals("[")){
							para = myCborParser.nextTextValue();
							type = myCborParser.nextTextValue();
							
							// Prüfe ob Typ supported wird.
							if (allowedTypes.contains(type)){
								paras.add(para);
								types.add(type);
							}else{
								errorOccurred = true;
							}
							
							// Nächstes Token muss Array schließen, ansonsten Fehler.
							myCborParser.nextToken();
							if (!myCborParser.getText().equals("]")){
								errorOccurred = true;
							}
						}else if (myCborParser.getText().equals("]")) {
							// CBOR Ende
						}else{
							// Kein (korrektes) Array in CBOR gefunden
							errorOccurred = true;
						}
					}
					
					if (!errorOccurred){
						if (database.registerClient(name, source_address, paras, types)){
							exchange.respond(ResponseCode.CREATED);
						}else{
							exchange.respond(ResponseCode.INTERNAL_SERVER_ERROR);
						}
					}else{
						exchange.respond(ResponseCode.BAD_REQUEST);
					}
					
				} catch (JsonParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}else{
				// Client nicht erlaubt - Speichere in tbl_unregistered
				database.storeUnregisteredClient(source_address);
				exchange.respond(ResponseCode.UNAUTHORIZED);
			}
		}	
    }

	public static class DataRessource extends CoapResource{
		private MySQL database;
    	public DataRessource(){
    		super("data");
    		this.database = new MySQL();
    
        	getAttributes().setTitle("Datenuebertragung");
    	}
    	
    	

		@Override
		public void handlePUT(CoapExchange exchange) {
			// TODO Auto-generated method stub
			
			// Ermittle letzte registrierte ID zur Source Address
			CBORFactory f = new CBORFactory();
			String source_address = exchange.getSourceAddress().getHostAddress();
			System.out.println("Received PUT on /data from "+source_address);
			boolean errorOccurred = false;
			
			List<String> values = new ArrayList<String>();
			if (database.doesClientAddressExist(source_address)){
				try {
					byte payload[] = exchange.getRequestPayload();
					CBORParser myCborParser = f.createParser(payload);
					
					//Springe zum ersten Token
					myCborParser.nextToken();
					
					// Erhalte Reihenfolge von String & Integer 
					List<String> types = database.getClientParamsTypeList(source_address);
					
					for (String type : types) {
						switch (type){
						case "string":
							String paraString = myCborParser.nextTextValue();
							values.add(paraString);
							break;
						case "integer":
							int paraInt = myCborParser.nextIntValue(0);
							values.add(Integer.toString(paraInt));
							break;
						case "float":
							myCborParser.nextToken();
							float paraFloat = myCborParser.getFloatValue();
							values.add(Float.toString(paraFloat));
							break;
						default:
								errorOccurred = true;
						}
					}
					
					// Unbekannter Typ oder falsche Reihenfolge
					if (errorOccurred){
						exchange.respond(ResponseCode.BAD_REQUEST);
					}else{
						// Füge Daten in Datenbank hinzu. Bei Fehler: INTERNAL_SERVER_ERROR
						if (database.addData(source_address, values)){
							exchange.respond(ResponseCode.CHANGED);
						}else{
							exchange.respond(ResponseCode.INTERNAL_SERVER_ERROR);
						}
					}
					
					
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					exchange.respond(ResponseCode.INTERNAL_SERVER_ERROR);
				}
				
			}else{
				exchange.respond(ResponseCode.UNAUTHORIZED);
			}
		}

    }
}
