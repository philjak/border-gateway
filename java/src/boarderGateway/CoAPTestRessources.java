package boarderGateway;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;
import com.fasterxml.jackson.dataformat.cbor.CBORParser;


public class CoAPTestRessources {
	

	public static class RegisterRessourceTest extends CoapResource{
		
    	public RegisterRessourceTest(){
    		super("register");
        	getAttributes().setTitle("Client Registrierung");
    	}

		
		@Override
		public void handlePOST(CoapExchange exchange) {			
			List<String> allowedTypes = Arrays.asList("string", "integer", "float");
			boolean errorOccurred = false;

			// TODO Auto-generated method stub
			CBORFactory f = new CBORFactory();
			String source_address = exchange.getSourceAddress().getHostAddress();
			System.out.println("Received POST on /register from "+source_address);
			
				try {
					// Lade erhaltenen Bytecode in CBOR Parser
					byte payload[] = exchange.getRequestPayload();
					CBORParser myCborParser = f.createParser(payload);
		
					List<String> paras = new ArrayList<String>();
					List<String> types = new ArrayList<String>();
					
					myCborParser.nextToken();
					myCborParser.nextToken();
					
					String name = myCborParser.getText();
					System.out.println("Name: "+name);
					
					// Lese Arrays aus: [ "Parameter Name" , "Datentyp"]
					String para = "";
					String type = "";
					
					// While Schleife, bis zum Ende der CBOR Nachricht 
					while (myCborParser.nextToken() != null){
						// Array Beginn
						if (myCborParser.getText().equals("[")){
							para = myCborParser.nextTextValue();
							type = myCborParser.nextTextValue();
							
							// Prüfe ob Typ supported wird.
							if (allowedTypes.contains(type)){
								paras.add(para);
								types.add(type);
								System.out.println("Added Para "+para+" of Type "+type);
							}else{
								errorOccurred = true;
							}
							
							// Nächstes Token muss Array schließen, ansonsten Fehler.
							myCborParser.nextToken();
							if (!myCborParser.getText().equals("]")){
								errorOccurred = true;
							}
						}else if (myCborParser.getText().equals("]")) {
							// CBOR Ende
						}else{
							// Kein (korrektes) Array in CBOR gefunden
							errorOccurred = true;
						}
					}
					
					if (!errorOccurred){
						exchange.respond(ResponseCode.CREATED);
					}else{
						exchange.respond(ResponseCode.BAD_REQUEST);
					}
					
				} catch (JsonParseException e) {
					// TODO Auto-generated catch block
					exchange.respond(ResponseCode.BAD_REQUEST);
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					exchange.respond(ResponseCode.BAD_REQUEST);
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					exchange.respond(ResponseCode.BAD_REQUEST);
					e.printStackTrace();
				}
		}	
    }

	public static class DataRessourceTest extends CoapResource{
    	public DataRessourceTest(){
    		super("data");
    
        	getAttributes().setTitle("Datenuebertragung");
    	}
    	
    	

		@Override
		public void handlePUT(CoapExchange exchange) {
			// TODO Auto-generated method stub
			
			// Ermittle letzte registrierte ID zur Source Address
			CBORFactory f = new CBORFactory();
			String source_address = exchange.getSourceAddress().getHostAddress();
			System.out.println("Received PUT on /data from "+source_address);
			
				try {
					byte payload[] = exchange.getRequestPayload();
					CBORParser myCborParser = f.createParser(payload);
					
					// Erhalte Reihenfolge von String & Integer 
					
					while (myCborParser.nextToken() != null){
						System.out.println(myCborParser.getValueAsString(""));
					}
					
					
					exchange.respond(ResponseCode.CHANGED);
						
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					exchange.respond(ResponseCode.INTERNAL_SERVER_ERROR);
				}
		}

    }
}
