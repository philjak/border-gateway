package boarderGateway;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class MySQL {
	
	private String db_server;
	private String db_port;
	private String db_user;
	private String db_dbname;
	private String db_pass;
	
	private String tbl_register;
	private String tbl_params;
	private String tbl_data;
	private String tbl_addresses;
	private String tbl_unregistered;
	
	private Connection conn = null;
	private Statement statement = null;
	
	public MySQL() {
		
		// Lade Datenbankverbindungsdaten aus MySQL.properties.
		try {
			Properties prop = new Properties();
			InputStream input = null;
			
			input = new FileInputStream("MySQL.properties");
			prop.load(input);
			
			this.db_server = prop.getProperty("db_server");
			this.db_port = prop.getProperty("db_port");
			this.db_user = prop.getProperty("db_user");
			this.db_dbname = prop.getProperty("db_dbname");
			this.db_pass = prop.getProperty("db_pass");
			
			this.tbl_register = prop.getProperty("tbl_register");
			this.tbl_params = prop.getProperty("tbl_params");
			this.tbl_data = prop.getProperty("tbl_data");
			this.tbl_addresses = prop.getProperty("tbl_addresses");
			this.tbl_unregistered = prop.getProperty("tbl_unregistered");

		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
		this.conn = this.connect();
	}
	
	private Connection connect(){
	
		try {
		    conn =
		       DriverManager.getConnection("jdbc:mysql://"+this.db_server+":"+this.db_port+"/"+this.db_dbname+"?" +
		                                   "user="+this.db_user+"&password="+this.db_pass);
		    
		    this.statement = (Statement) this.conn.createStatement();
		    return conn;
		
		} catch (SQLException ex) {
		    // handle any errors
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		    
		    return null;
		}
	}
	
	public boolean isClientAllowed(String source_address){
		String query = "";
		query = "SELECT * FROM "+this.tbl_addresses+" WHERE address = '"+source_address+"'";
		ResultSet rs;
		try {
			rs = this.statement.executeQuery(query);
			return rs.next();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return false;
		}
		
	}
	
	public boolean registerClient(String name, String clientAddress, List<String> params, List<String> types){
		// TODO: Prüfe ob Client bereits registriert?
		String query = "";
		try {
			query = "INSERT INTO "+this.tbl_register+"(clientAddress, name) VALUES('"+clientAddress+"', '"+name+"')";
			this.statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
			int lastID = this.getLastInsertedID(this.statement.getGeneratedKeys());
			
			if (lastID > 0){
				for ( int i = 0; i<params.size(); i++){
					query =  "INSERT INTO "+this.tbl_params+"(fid_register, param, type) VALUES("+lastID+", '"+params.get(i)+"', '"+types.get(i)+"')";
					this.statement.executeUpdate(query);
				}
				return true;
			}else{
				return false;
			}
			
		} catch (SQLException ex) {
		    // handle any errors
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		    return false;
		}
	}
	
	public boolean addData(String clientAddress, List<String> values){
		String query = "";
		
		
		try {
			// Erhalte aktuellsten Primary Key der entsprechenden clientID
			query = "SELECT * FROM "+this.tbl_register+" WHERE clientAddress = '"+clientAddress+"' ORDER BY ID DESC LIMIT 1";
			ResultSet rs = this.statement.executeQuery(query);
			rs.next();
			int id = rs.getInt(1);
			
			// Erhalte ParameterID-Liste zur entsprechenden Client ID
			List<Integer> parameterIDs = new ArrayList<Integer>();
			query = "SELECT * FROM "+this.tbl_params+" WHERE fid_register = "+id;
			rs = this.statement.executeQuery(query);
			while (rs.next()){
				parameterIDs.add(rs.getInt("id"));
			}
			
			// Wenn Parameter- und Werteliste gleich groß, füge in Datenbank ein
			if (values.size() == parameterIDs.size()){
				for (int i = 0; i < values.size(); i++){
					query = "INSERT INTO "+this.tbl_data+"(fid_parameter, value) VALUES("+parameterIDs.get(i).toString()+", '"+values.get(i).toString()+"')";
					this.statement.executeUpdate(query);
				}
			}else{
				return false;
			}
			
		} catch (SQLException ex) {
			// handle any errors
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		    return false;
		}
		
		
		return true;
	}
	
	public boolean doesClientAddressExist(String clientAddress){
		String query = "SELECT * FROM "+this.tbl_register+" WHERE clientAddress = '"+clientAddress+"'";
		try {
			ResultSet rs = this.statement.executeQuery(query);
			// True if not empty
			return rs.next();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return false;
		}
		
	}
	
	public List<String> getClientParamsTypeList(String clientAddress){
		List<String> parameters = new ArrayList<String>();
		String query = "SELECT * FROM "+this.tbl_register+" WHERE clientAddress = '"+clientAddress+"' ORDER BY id DESC LIMIT 1";
		ResultSet rs;
		String clientID = "";
		try {
			rs = this.statement.executeQuery(query);
			while (rs.next()){
				clientID = Integer.toString(rs.getInt("id"));
			}
			
			query = "SELECT * FROM "+this.tbl_params+" WHERE fid_register = "+clientID;
			rs = this.statement.executeQuery(query);
			while (rs.next()){
				parameters.add(rs.getString("type"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return parameters;
	}
	
	public void storeUnregisteredClient(String clientAddress){
		String query = "SELECT * FROM "+this.tbl_unregistered+" WHERE clientAddress = '"+clientAddress+"'";
		try {
			ResultSet rs = this.statement.executeQuery(query);
			// True if not empty
			if (!rs.next()){
				query = "INSERT INTO "+this.tbl_unregistered+"(clientAddress) VALUES('"+clientAddress+"')";
				this.statement.executeUpdate(query);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// Do Nothing
		}
			
	}
	
	private int getLastInsertedID(ResultSet keys){
		try {
			keys.next();
			return keys.getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return 0;
		}
		
	}

}
