package boarderGateway;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import org.eclipse.californium.core.CoapServer;

import boarderGateway.CoAPRessources.*;
import boarderGateway.CoAPTestRessources.*;


public class CoAPServer {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	

		CoapServer server = new CoapServer();
		
		
		if (args.length > 0){
			// Starte Testserver wenn erstes Argument "test" ist. Ansonsten normaler Start.
			if (args[0].equals("test")){
				server.add(new RegisterRessourceTest());
		        server.add(new DataRessourceTest());
			}else{
				createProperties();
				server.add(new RegisterRessource());
		        server.add(new DataRessource());
			}
		}else{
			// Normaler Start wenn kein Argument gegeben
			createProperties();
			server.add(new RegisterRessource());
	        server.add(new DataRessource());
		}
		
        server.start();
	}
	
	// Überprüfe ob MySQL.properties existiert. Falls nein, erstelle ein Default-Set
	private static void createProperties(){
		File f = new File("MySQL.properties");
		if(!f.exists()) { 
			System.out.println("Creating MySQL.properties");
		
			try {
				Properties prop = new Properties();
				OutputStream output = null;
				
				output = new FileOutputStream("MySQL.properties");
	
				// set the properties value		
				prop.setProperty("db_server", "localhost");
				prop.setProperty("db_port", "3306");
				prop.setProperty("db_user", "root");
				prop.setProperty("db_dbname", "iot");
				prop.setProperty("db_pass", "");
				
				prop.setProperty("tbl_register", "tbl_register");
				prop.setProperty("tbl_params", "tbl_parameter");
				prop.setProperty("tbl_data", "tbl_data");
				prop.setProperty("tbl_addresses", "tbl_addresses");
				prop.setProperty("tbl_unregistered", "tbl_unregistered");
	
				// save properties to project root folder
				prop.store(output, null);
	
			} catch (IOException io) {
				io.printStackTrace();
			}
		}else{
			System.out.println("Found MySQL Config at "+f.getAbsolutePath());
		}
	}

}
