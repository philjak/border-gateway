package boarderGateway;
import java.io.ByteArrayOutputStream;
import java.util.Random;

import org.eclipse.californium.core.CoapClient;

import co.nstant.in.cbor.CborBuilder;
import co.nstant.in.cbor.CborEncoder;
import co.nstant.in.cbor.CborException;

import org.apache.commons.codec.binary.Hex;


public class CoAPClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] encodedBytes = null;
		CoapClient client = null;
		
		String address = "localhost";
		if (args.length > 0){
			address = args[0];
		}
		
//		try {
//			new CborEncoder(baos).encode(new CborBuilder()
//			.addArray()
//				.add(randomString(15))
//			    .addArray()                 // add array
//			        .add("My Float Parameter")
//			        .add("float")
//			        .end()
//			    .addArray()
//			    	.add("My String Parameter")
//			    	.add("string")
//			    	.end()
//			    .addArray()
//			    	.add("My Integer Parameter")
//			    	.add("integer")
//			    	.end()
//			    .end()
//			    .build());
//			
//			System.out.println("Byte: "+Hex.encodeHexString(baos.toByteArray()));
//			encodedBytes = baos.toByteArray();
//			
//			client = new CoapClient("coap://"+address+"/register");
//			client.post(encodedBytes, 0);
//			
//			} catch (CborException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
		
		
		
		
			try {
				new CborEncoder(baos).encode(new CborBuilder()
				.addArray()
					.add(randomFloat(0, 50))
					.add(randomString(10))
					.add(randomInt(0,30))
				    .end()
				    .build());
				
				System.out.println("Byte: "+Hex.encodeHexString(baos.toByteArray()));
				encodedBytes = baos.toByteArray();
				
				client = new CoapClient("coap://"+address+"/data");
				client.put(encodedBytes, 0);
				
		} catch (CborException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private static String randomString(int length){
		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    sb.append(c);
		}
		return sb.toString();
	}
	
	private static Float randomFloat(float min, float max){
		Random rand = new Random();

		float randomFloat = rand.nextFloat() * (max - min) + min;
		return randomFloat;
	}
	
	private static int randomInt(int min, int max){
		int randomInt = min + (int)(Math.random() * max); 
		return randomInt;
	}

}
