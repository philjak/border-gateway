# Dokumentation
## Einleitung
Das Boarder-Gateway stellt zwei CoAP Ressourcen zur Verfügung. Die Ressource **/register** wird dabei von CoAP Clients verwendet, um sich am BG zu registrieren.
Bei der Registrierung eines CoAP Clients muss eine Liste von Parameternamen mitgesendet werden, bspw. *["Temperatur vorne", "Temperatur hinten", "Luftfeuchtigkeit"]*. 
    
Nach der Registrierung kann ein CoAP Client die Ressource **/data** dafür verwenden, um regelmäßig Werte den entsprechend registrierten Parameter zuzuweisen. Dazu muss
der CoAP Client ausschließlich die Werte in der Reihenfolge übersenden, in welcher die Parameter registriert wurden.
  
  
Alle Ressourcen erwarten als Payload CBOR. 

## Schnittstelle 
### Registrierung
```
coap://<adresse>:5683/register
```

#### POST
##### CBOR Struktur
```
1. Parameter = Client Name
2. Parameter = Array für ersten Parameter
   2.1 = Parameter Name
   2.2 = Datentyp
N. Parameter = Array für N. Parameter
   N.1 = Parameter Name
   N.2 = Parameter Typ
```

##### Beispiel
```
[ 
  "Smart Environment",
  ["Temp-Sensor 1", "integer"],
  ["Temp-Sensor 2", "float"],
  ["Irgendein-Anderer-Wert", "string"]
]
```
##### Response Codes
```
2.01 = Registrierung erfolgreich
4.00 = Anfrage konnte nicht verarbeitet werden (CBOR fehlerhaft?)
4.01 = Registrierung nicht erlaubt
4.04 = Ressource nicht bekannt
4.05 = Methode nicht erlaubt
5.00 = Server- / Datenbankfehler
```

### Datenübertragung
```
coap://<adresse>:5683/data
```
#### PUT
##### CBOR Struktur
```
1. Parameter = Wert des 1. bei der Registrierung übermittelten Parameter 
2. Parameter = Wert des 2. bei der Registrierung übermittelten Parameter 
N. Parameter = Wert des N. bei der Registrierung übermittelten Parameter 
```
##### Beispiel
```
[
   12,
   42.4,
   "OK"
]
```
##### Response Codes
```
2.04 = Werte gespeichert
4.00 = Anfrage konnte nicht verarbeitet werden (CBOR fehlerhaft?)
4.01 = ID nicht registriert oder nicht erlaubt
4.04 = Ressource nicht bekannt
4.05 = Methode nicht erlaubt
5.00 = Server- / Datenbankfehler
```