<?php
include("include/config.php");
require_once("classes/SQL.php");
include("include/_head.php");

?>

<!-- Page Header -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Dashboard
                </h1>
            </div>
        </div>

<?php
$sql = new SQL($DBHOST, $DBUSER, $DBPASS, $DBNAME);
$link = $sql->getLink();
$graphs = array();

// Alle Clients mit dem Timestamp ihres zuletzt gesendeten Parameter
$query = "SELECT clientAddress, name, max(".$TBL_DATA.".timestamp) as date, fid_register FROM ".$TBL_ADDRESSES." INNER JOIN ((".$TBL_REGISTER." INNER JOIN ".$TBL_PARAMETER." ON ".$TBL_REGISTER.".id = ".$TBL_PARAMETER.".fid_register) INNER JOIN ".$TBL_DATA." ON ".$TBL_PARAMETER.".id = ".$TBL_DATA.".fid_parameter) ON ".$TBL_ADDRESSES.".address = ".$TBL_REGISTER.".clientAddress WHERE fid_register IN (SELECT max(id) FROM ".$TBL_REGISTER." group by clientAddress) AND ".$TBL_DATA.".timestamp > (NOW() - INTERVAL ".$DEVICEOFFLINETIME." MINUTE) group by fid_register";

$result = $link->query($query);
while($row = $result->fetch_assoc()) {

    echo '<!-- Projects Row -->
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header">'.$row["name"].'
                </h3>
            </div>
        </div>
        <div class="row">';

    
    // Alle Parameter und Werte zu einem konkreten Client (von der äußeren Schleife kommend)
    $query2 = "SELECT type, fid_parameter, param, value, DATE_FORMAT(".$TBL_DATA.".timestamp, '%d.%m.%Y %H:%i:%s') as date FROM (".$TBL_REGISTER." INNER JOIN ".$TBL_PARAMETER." ON ".$TBL_REGISTER.".id = ".$TBL_PARAMETER.".fid_register) INNER JOIN ".$TBL_DATA." ON ".$TBL_PARAMETER.".id = ".$TBL_DATA.".fid_parameter WHERE ".$TBL_REGISTER.".id = ".$row["fid_register"]." AND ".$TBL_DATA.".id IN (SELECT max(id) FROM ".$TBL_DATA." group by fid_parameter) ORDER BY ".$TBL_PARAMETER.".id";
    $result2 = $link->query($query2);

    while($row2 = $result2->fetch_assoc()) {


        echo '
            <div class="col-md-'.$THUMBSIZE.' portfolio-item">';

            if ($row2["type"] == "string"){
                echo '
                <a href="details.php?param='.$row2["fid_parameter"].'">
                    <img class="img-responsive" src="img/table.png" alt="">
                </a>';
            }else{
                echo '
                <a href="details.php?param='.$row2["fid_parameter"].'">
                    <img class="img-responsive" src="graph.php?param='.$row2["fid_parameter"].'" alt="" id="p'.$row2["fid_parameter"].'">
                </a>';
                array_push($graphs, $row2["fid_parameter"]);
            }
                echo '
                <h4>
                    <a href="details.php?param='.$row2["fid_parameter"].'">'.$row2["param"].'</a>
                </h4>
                <p><b>Value: </b> <span id="v'.$row2["fid_parameter"].'">'.$row2["value"].'</span> <br> <small><i>Last update: <span id="u'.$row2["fid_parameter"].'">'.$row2["date"].'</span></i></small></p>
            </div>';
    }

    echo '</div>
        <!-- /.row -->';
}



// Refreshfunktionen
echo '
<script>
setInterval(function() {
    ';

    foreach ($graphs as &$param){
        echo "document.getElementById('p".$param."').src = 'graph.php?param=".$param."&rand=' + Math.random();
        ";
        echo '$.post("api/lastUpdate.php",
            {
                param: "'.$param.'"
            },
            function(data, status){
                var response = $.parseJSON(data);
                $("#u'.$param.'").text(response.update);
                $("#v'.$param.'").text(response.value);
            });';
    }

    if (count($graphs) == 0){
        echo 'location.reload();';
    }
echo '    
}, '.$REFRESHINTERVAL.');
</script>
';
?>
        



<?php
include("include/_foot.php");
?>