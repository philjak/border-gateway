<?php
	
class SQL{

	private $link;

	function __construct($dbHost, $dbUser, $dbPass, $dbName){
		$this->link = new mysqli($dbHost, $dbUser, $dbPass, $dbName);
		$this->link->set_charset("utf8");
	}

	public function getLink(){
		return $this->link;
	}

}


?>