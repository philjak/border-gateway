<?php

// Database
$TBL_PARAMETER = "tbl_parameter";
$TBL_REGISTER = "tbl_register";
$TBL_DATA = "tbl_data";
$TBL_ADDRESSES = "tbl_addresses";
$TBL_UNREGISTERED = "tbl_unregistered";
$DBHOST = "localhost";
$DBUSER = "root";
$DBPASS = "";
$DBNAME = "iot";

// Dashboard
$DEVICEOFFLINETIME =  24; // Nach X Stunden Inaktivität vom Dashboard entfernen
$REFRESHINTERVAL = 25; // Alle X Sekunden Daten im Dashboard neu laden
$THUMBSIZE = 4; // Größe der Vorschaubilder - 1 min, 12 max

// Graphs
$GRAPHPOINTS = 50; // Anzahl der X letzten Messungen im Graphen

// Details
$TABLELINESPERPAGE = 50; // Anzahl der Tabelleneinträge pro Seite

// Admin
$ADMINPASSWORD = "12345678";


// +++++++++++++++++ DONT DO ANY CHANGES AFTER THIS LINE ++++++++++++++++++

$DEVICEOFFLINETIME = $DEVICEOFFLINETIME * 60;
$REFRESHINTERVAL = $REFRESHINTERVAL * 1000;
$GRAPHPOINTS = $GRAPHPOINTS * (-1);
?>