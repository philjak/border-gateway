<?php
include("include/config.php");
require_once("classes/SQL.php");
include("include/_head.php");

?>

<?php
$sql = new SQL($DBHOST, $DBUSER, $DBPASS, $DBNAME);
$link = $sql->getLink();

if (isset($_GET["param"])){
    $query = "SELECT * FROM (".$TBL_REGISTER." INNER JOIN ".$TBL_PARAMETER." ON ".$TBL_REGISTER.".id = ".$TBL_PARAMETER.".fid_register) INNER JOIN ".$TBL_DATA." ON ".$TBL_PARAMETER.".id = ".$TBL_DATA.".fid_parameter WHERE ".$TBL_DATA.".fid_parameter = ".$_GET["param"]." LIMIT 1";

        $result = $link->query($query);
        while($row = $result->fetch_assoc()) {
            $name = $row["name"];
            $param = $row["param"];
            $type = $row["type"];
        }
        echo '
        <!-- Page Header -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">'.$name.' <small>'.$param.'</small>
                </h1>
            </div>
        </div>';


        echo '
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="index.php" class="btn btn-success" role="button">back</a>
                <br><br>
            </div>
        </div>
        ';

        if ($type != "string"){
            echo '
        <div class="row">
            <div class="col-md-12 portfolio-item">
                <img class="img-responsive" src="graph.php?param='.$_GET["param"].'&details=yes">
            </div>
        </div>';
        }

        echo '
        <div class="table-responsive">          
          <table class="table table-striped">
            <thead>
              <tr>
                <th>'.$param.'</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>
        ';
        
        $query = "SELECT count(*) as amount FROM ".$TBL_DATA." WHERE ".$TBL_DATA.".fid_parameter = ".$_GET["param"];
        $result = $link->query($query);
        while($row = $result->fetch_assoc()) {
            $amount = $row["amount"];
        }

        $seiten = ceil($amount/$TABLELINESPERPAGE);
        if (isset($_GET["page"])){
            $page = $_GET["page"];
            if ($page > $seiten){
                $page = $seiten;
            }elseif ($page < 1){
                $page = 1;
            }
        }else{
            $page = 1;
        }
        


        $query = "SELECT ".$TBL_DATA.".id as id, ".$TBL_PARAMETER.".param as param, ".$TBL_DATA.".value as value, DATE_FORMAT(".$TBL_DATA.".timestamp, '%d.%m.%Y %H:%i:%s') as date FROM (".$TBL_REGISTER." INNER JOIN ".$TBL_PARAMETER." ON ".$TBL_REGISTER.".id = ".$TBL_PARAMETER.".fid_register) INNER JOIN ".$TBL_DATA." ON ".$TBL_PARAMETER.".id = ".$TBL_DATA.".fid_parameter WHERE ".$TBL_DATA.".fid_parameter = ".$_GET["param"]." ORDER BY ".$TBL_DATA.".id DESC LIMIT ".$TABLELINESPERPAGE." OFFSET ".(($page-1)*$TABLELINESPERPAGE);

        $result = $link->query($query);
        while($row = $result->fetch_assoc()) {
        echo '    
              <tr>
                <td>'.$row["value"].'</td>
                <td>'.$row["date"].'</td>
              </tr>';
        }

        echo '
            </tbody>
          </table>
        </div>';
        

        echo '
        <!-- Pagination -->
        <div class="row text-center">
            <div class="col-lg-12">
                <ul class="pagination">
                    <li>
                        <a href="'.$_SERVER["PHP_SELF"].'?param='.$_GET["param"].'&page='.($page-1).'">&laquo;</a>
                    </li>
                    ';
                    for ($i = 1; $i<=$seiten; $i++){
                        if ($i == $page){
                            echo '<li class="active">';
                        }else{
                            echo '<li>';
                        }
                        echo '<a href="'.$_SERVER["PHP_SELF"].'?param='.$_GET["param"].'&page='.$i.'">'.$i.'</a>
                        </li>';
                    }
                    echo '
                    <li>
                        <a href="'.$_SERVER["PHP_SELF"].'?param='.$_GET["param"].'&page='.($page+1).'">&raquo;</a>
                    </li>
                </ul>
            </div>
        </div>';



        echo '
         <div class="row">
            <div class="col-md-12 text-center">
                <a href="index.php" class="btn btn-success" role="button">back</a>
                <br><br>
            </div>
        </div>'
        ;
        
}
?>
        



<?php
include("include/_foot.php");
?>