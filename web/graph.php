<?php // content="text/plain; charset=utf-8"
require_once ('include/config.php');
require_once ('classes/jpgraph/jpgraph.php');
require_once ('classes/jpgraph/jpgraph_line.php');
require_once ('classes/SQL.php');


if (isset($_GET["param"])){
	if (isset($_GET["details"]) && $_GET["details"] == "yes"){
		$showDetails = true;
	}else{
		$showDetails = false;
	}

		$sql = new SQL($DBHOST, $DBUSER, $DBPASS, $DBNAME);
		$link = $sql->getLink();

		$dataY = array();
		$dataX = array();

		$result = $link->query("SELECT * FROM ".$TBL_PARAMETER." INNER JOIN ".$TBL_REGISTER." ON ".$TBL_PARAMETER.".fid_register = ".$TBL_REGISTER.".id WHERE ".$TBL_PARAMETER.".id = ".$_GET["param"]);
		while($row = $result->fetch_assoc()) {
			$name = $row["name"];
			$param = $row["param"];
		}

		$result = $link->query("SELECT fid_parameter, value, DATE_FORMAT(timestamp, '%d.%m.%Y %H:%i:%s') as date FROM ".$TBL_DATA." WHERE fid_parameter = ".$_GET["param"]);
		while($row = $result->fetch_assoc()) {
			array_push($dataY, $row["value"]);
			array_push($dataX, $row["date"]);
		}

		// Beschränke die Anzahl der Punkte auf X Achse 
		$dataY = array_slice($dataY, $GRAPHPOINTS);
		$dataX = array_slice($dataX, $GRAPHPOINTS);

		// Reduziere Anzahl der Daten auf X-Achse
		// 10 Prozent der Gesamtanzahl sind die Marken, die zwischen der ersten und letzten Marke hinzugefügt werden sollen.
		// Beispiel: 20 Messungen => 
		if (count($dataX) > 2){
			// 10% an weiteren Markern

			// Bestimme 10 Prozent. Beispiel: 20 Messungen => 2 => Aufteilung in 3 gleichgroße Teile
			// 20 / 3 = 7. Marker bei 7 und 14.
			$teile = round(0.1 * count($dataX)) + 1; // = 3
			$anzahl = round(count($dataX) / $teile); // = 7
			$marker = $anzahl;

			for ($i = 1; $i<count($dataX)-1; $i++){
				if ($showDetails){
					if ($i == $marker){
						$marker = $marker + $anzahl;
					}else{
						$dataX[$i] = "";
					}
				}else{
					$dataX[$i] = "";
				}
				
			}
		}
		if (!$showDetails) {
			$dataX[0] = "";
			$dataX[count($dataX)-1] = "";
		}
		

		// Setup the graph
		$graph = new Graph(1024,600);
		$graph->SetScale("textlin");

		$theme_class=new UniversalTheme;

		$graph->SetTheme($theme_class);
		
		$graph->SetBox(true);
		


		
		$graph->img->SetAntiAliasing(false);

		$graph->yaxis->HideZeroLabel();
		$graph->yaxis->HideLine(false);
		$graph->yaxis->HideTicks(true,true);

		$graph->xgrid->Show();
		$graph->xgrid->SetLineStyle("solid");
		$graph->ygrid->SetLineStyle('dotted');
		$graph->xaxis->SetLabelAngle(35);
		$graph->xaxis->SetTickLabels($dataX);
		$graph->xgrid->SetColor('#E3E3E3');

		// Create the first line
		$p1 = new LinePlot($dataY);
		$graph->Add($p1);
		$p1->SetColor("#6495ED");
		$p1->SetWeight(5); 
		

		

		$graph->legend->SetFrameWeight(1);


		if ($showDetails){
			$graph->title->Set($name);
			$graph->SetMargin(130,100,40,100);
			$graph->yaxis->HideTicks(false,false);
			$graph->xgrid->SetLineStyle("solid");
			$graph->img->SetAntiAliasing(true);

		}else{
			$graph->yaxis->SetFont(FF_ARIAL,FS_NORMAL,14);
			$graph->SetMargin(60,20,20,20);
		}

		// Output line
		if (count($dataY) > 1){
			$graph->Stroke();
		}else{
			header('Content-type: image/png');
			readfile("img/nodata.png");
		}
}
?>