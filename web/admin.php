<?php
session_start();
include("include/config.php");
require_once("classes/SQL.php");
include("include/_head.php");


$loggedIn = false;
$loginFailed = true;
$logout = false;

if (isset($_GET["action"])){
	if ($_GET["action"] == "logout"){
		session_destroy();
		$logout = true;
	}
}

if (isset($_POST["password"])){
	$_SESSION["login"] = $_POST["password"];
}else{
	$loginFailed = false;
}

if (isset($_SESSION["login"]) && !$logout){
	$login = $_SESSION["login"];

	if ($login == $ADMINPASSWORD){
		$loggedIn = true;
		$loginFailed = false;
	}
}

if ($loginFailed){
	echo '<div class="alert alert-danger">
  <strong>Error!</strong> Entered password was not correct.
</div>';
}


if (!$loggedIn){
	// Display form
	echo '
	 <form role="form" action="'.$_SERVER["PHP_SELF"].'" method="POST">
	  <div class="form-group">
	    <label for="password">Password:</label>
	    <input type="password" class="form-control" name="password">
	  </div>
	  <button type="submit" class="btn btn-success">Login</button>
	</form>
';
}else{
	$sql = new SQL($DBHOST, $DBUSER, $DBPASS, $DBNAME);
	$link = $sql->getLink();
	$exist = false;



	if (isset($_GET["allow"])){
		$allow = $_GET["allow"];
		$query = "SELECT * FROM ".$TBL_UNREGISTERED." WHERE id = ".$allow;
		$result = $link->query($query);
	        while($row = $result->fetch_assoc()) {
	        	$clientAddress = $row["clientAddress"];
	        	$exist = true;
	        }
	    if ($exist){
	    	$query = "INSERT INTO ".$TBL_ADDRESSES."(address) VALUES('".$clientAddress."')";
		    $result = $link->query($query);

		    $query = "DELETE FROM ".$TBL_UNREGISTERED." WHERE id = ".$allow;
		    $result = $link->query($query);
	    }
	    

	}elseif (isset($_GET["remove"])) {
		$remove = $_GET["remove"];
		$query = "SELECT * FROM ".$TBL_ADDRESSES." WHERE id = ".$remove;
		$result = $link->query($query);
	    while($row = $result->fetch_assoc()) {
	    	$exist = true;
	    }
	    if ($exist){
	    	$query = "DELETE FROM ".$TBL_ADDRESSES." WHERE id = ".$remove;
	    	$result = $link->query($query);
	    }
		
	}

	?>

	<!-- Page Header -->
	        <div class="row">
	            <div class="col-lg-12">
	                <h1 class="page-header">Admin
	                </h1>
	            </div>
	        </div>

	         <div class="row">
	            <div class="col-lg-12">
	                <h3 class="page-header">Unknown Clients
	                </h3>
	            </div>
	        </div>
	        <div class="table-responsive">          
	          <table class="table table-striped">
	            <thead>
	              <tr>
	                <th class="col-md-6">Client Address</th>
	                <th class="col-md-6">Action</th>
	              </tr>
	            </thead>
	            <tbody>


	<?php
	$query = "SELECT * FROM ".$TBL_UNREGISTERED."";

	        $result = $link->query($query);
	        while($row = $result->fetch_assoc()) {
	        echo '    
	              <tr>
	                <td class="col-md-6">'.$row["clientAddress"].'</td>
	                <td class="col-md-6"> <a href="'.$_SERVER["PHP_SELF"].'?allow='.$row["id"].'" class="btn btn-success" role="button">Allow access</a></td>
	              </tr>';
	        }

	        echo '
	            </tbody>
	          </table>
	        </div>';

	?>

	         <div class="row">
	            <div class="col-lg-12">
	                <h3 class="page-header">Allowed Clients
	                </h3>
	            </div>
	        </div>
	        <div class="table-responsive">          
	          <table class="table table-striped">
	            <thead>
	              <tr>
	                <th class="col-md-6">Client Address</th>
	                <th class="col-md-6">Action</th>
	              </tr>
	            </thead>
	            <tbody>

	<?php
	$query = "SELECT * FROM ".$TBL_ADDRESSES."";

	        $result = $link->query($query);
	        while($row = $result->fetch_assoc()) {
	        echo '    
	              <tr>
	                <td class="col-md-6">'.$row["address"].'</td>
	                <td class="col-md-6"> <a href="'.$_SERVER["PHP_SELF"].'?remove='.$row["id"].'" class="btn btn-danger" role="button">Remove access</a></td>
	              </tr>';
	        }

	        echo '
	            </tbody>
	          </table>
	        </div>';

	        echo '
        <div class="row">
            <div class="col-md-12 text-center">
            <br><br><br>
                <a href="'.$_SERVER["PHP_SELF"].'?action=logout" class="btn btn-warning" role="button">Logout</a>
                <br><br>
            </div>
        </div>
        ';
}



   

include("include/_foot.php");
?>